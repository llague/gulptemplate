var gulp = require('gulp'),
    browserify = require('gulp-browserify'),
    sass = require('gulp-ruby-sass'),
    argv = require('yargs').argv,
    connect = require('gulp-connect'),
    cache = require('gulp-cached');

gulp.task('connect', function () {
  connect.server({
    livereload: true,
    port: 8005,
    root: './dist'
  });
});

gulp.task('reload', function () {
  gulp.src('./dist/**/*.*')
    .pipe(connect.reload());
});

gulp.task('scripts', function () {
  gulp.src('./src/js/*.js')
    .pipe(browserify({
      insertGlobals: true,
      debug: !argv.production
    }))
    .pipe(gulp.dest('./dist/js/'));
});

gulp.task('sass', function () {
  gulp.src('./src/scss/*.scss')
    .pipe(sass())
    .pipe(gulp.dest('./dist/css'))
});

gulp.task('html', function () {
  gulp.src('./src/*.html')
    .pipe(gulp.dest('./dist/'));
});

gulp.task('img', function () {
  gulp.src('./src/img/*.*')
    .pipe(gulp.dest('./dist/img/'));
});

gulp.task('watch', function () {
  gulp.watch(['./src/*.html'], ['html']);
  gulp.watch(['./src/js/*.js'], ['scripts']);
  gulp.watch(['./src/scss/*.scss'], ['sass']);
  gulp.watch(['./src/img/*.*'], ['img']);
  gulp.watch(['./dist/**/*.*'], ['reload']);
});

gulp.task('default', ['connect', 'scripts', 'sass', 'html', 'img', 'watch']);